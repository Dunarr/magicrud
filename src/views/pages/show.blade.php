@extends("admin.base")
@section('title', $label.' '.$entity->id)
@section('body')
    {{ $entity }}
    <a href="{{ route($name.'.edit', [$entity->id]) }}" class="btn btn-primary">
        Modifier
    </a>
@endsection
@section('breadcrumb', $entity->id)
@section('prevLink')
    <a href="{{ route($name.'.index')}}">{{ \Illuminate\Support\Pluralizer::plural($label) }}</a>
@endsection
