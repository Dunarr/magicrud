<div class="form-group">
    <label for="{{ $id }}">{{ $label }}</label>
    {!! Form::password($id, ['class' => "form-control square-input mt-15",'id' => $id]) !!}
    @error($id)
    <div class="text-danger">{{$message}}</div>
    @enderror
</div>
