<?php

namespace Dunarr\MagiCrud;

use Dunarr\MagiCrud\Controllers\CRUDController;
use Dunarr\MagiCrud\Middleware\ResourceManager;
use Illuminate\Routing\Router;
use Illuminate\Support\ServiceProvider;

class MagiCrudServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->make(CRUDController::class);
        $this->publishes([
            __DIR__ . '/config/magicrud.php' => config_path('magicrud.php'),
        ]);
        $this->mergeConfigFrom(
            __DIR__ . '/config/magicrud.php', 'magicrud'
        );
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot(Router $router)
    {
        $router->aliasMiddleware('ressource_manager', ResourceManager::class);
        $this->loadViewsFrom(__DIR__ . '/views', 'magicrud');
        $this->publishes([
            __DIR__ . '/views' => resource_path('views/vendor/magicrud'),
        ]);
        $this->loadRoutesFrom(__dir__ . '/routes/web.php');
        $this->publishes([
            __DIR__.'/../assets' => public_path('vendor/magicrud'),
        ], 'public');
    }
}
