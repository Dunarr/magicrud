<div class="form-group">
    <label for="{{ $id }}">{{ $label }}</label>
    {!! Form::textArea($id,null, ['class' => "form-control",'id' => $id]) !!}
    @error($id)
    <div class="text-danger">{{$message}}</div>
    @enderror
</div>
