<div class="form-group">
    <label for="{{ $id }}">{{ $label }}</label>
    {!! Form::select($id.($multiple?'[]':''), $choices, null, ['class' => "form-control ".(empty($gallery)?'select2':'gallery-selector'),'id' => $id, 'multiple' => $multiple]) !!}
    @error($id)
    <div class="text-danger">{{$message}}</div>
    @enderror
</div>
