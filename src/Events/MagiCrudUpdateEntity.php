<?php

namespace Dunarr\MagiCrud\Events;

use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Queue\SerializesModels;

class MagiCrudUpdateEntity
{
    use SerializesModels;
    public $entity;
    public $request;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(Model $entity, Request $request)
    {
        $this->entity = $entity;
        $this->request = $request;
    }
}
