<?php

namespace Dunarr\MagiCrud\Events;

use Illuminate\Queue\SerializesModels;

class MagiCrudGenerateForm
{
    use SerializesModels;

    public $resource;
    public $form;
    public $formOptions;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(Array $resource, Array $form, Array $formOptions)
    {
        $this->resource = $resource;
        $this->form = $form;
        $this->formOptions = $formOptions;
    }
}
