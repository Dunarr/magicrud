<?php


namespace Dunarr\MagiCrud\Controllers;


use Carbon\Carbon;
use Dunarr\MagiCrud\Composers\MagiMenuComposer;
use Dunarr\MagiCrud\Events\MagiCrudDisplayIndex;
use Dunarr\MagiCrud\Events\MagiCrudGenerateForm;
use Dunarr\MagiCrud\Events\MagiCrudIndex;
use Dunarr\MagiCrud\Events\MagiCrudUpdateEntity;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Storage;
use View;

class CRUDController
{

    public function __construct(Request $request)
    {
        View::composers([
            MagiMenuComposer::class => ['magicrud::base']
        ]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $resource = $request->attributes->get('resource');
        event($event = new MagiCrudDisplayIndex($request, $resource));
        $resource = $event->resource;
        $request = $event->request;
        $entities = $resource['model']::orderBy('id', 'DESC')->paginate(10);
        $label = $resource['label'];
        $uri = $resource['URI'];
        return view('magicrud::pages.index', compact('entities', 'label', 'uri'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create(Request $request)
    {
        $resource = $request->attributes->get('resource');
        $formData = $this->generateForm($resource);
        $form = $formData['form'];
        $formOptions = $formData['formOptions'];
        $label = $resource['label'];
        $uri = $resource['URI'];
        return view('magicrud::pages.create', compact('label', 'uri', 'form', 'formOptions'));
    }

    protected function generateForm($resource)
    {
        $form = $resource['form'] ?? [];
        $formOptions = ['files' => false];
        foreach ($form as $key => $field) {
            if (isset($field['class'])) {
                $form[$key]['choices'] = $field['class']::pluck($field['select-label'], 'id');
                unset($form[$key]['class']);
                unset($form[$key]['select-label']);
            }
            if ($field['type'] === 'file') {
                $formOptions ['files'] = true;
            }
        }
        event($event = new MagiCrudGenerateForm($resource, $form, $formOptions));
        $form = $event->form;
        $formOptions = $event->formOptions;
        $resource = $event->resource;
        return compact('formOptions', 'form');
    }

    /**
     * Display the specified resource.
     *
     * @param  $id
     * @return Response
     */
    public function show(Request $request, $resource, $id)
    {
        $resource = $request->attributes->get('resource');
        $entity = $resource['model']::findOrFail($id);
        $label = $resource['label'];
        $uri = $resource['URI'];
        return view('magicrud::pages.show', compact('entity', 'label', 'uri'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  $id
     * @return Response
     */
    public function edit(Request $request, $resource, $id)
    {
        $resource = $request->attributes->get('resource');
        $entity = $resource['model']::findOrFail($id);
        $formData = $this->generateForm($resource);
        $form = $formData['form'];
        $formOptions = $formData['formOptions'];
        $label = $resource['label'];
        $uri = $resource['URI'];
        return view('magicrud::pages.edit', compact('entity', 'form', 'label', 'uri', 'formOptions'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  $id
     * @return Response
     */
    public function destroy(Request $request, $resource, $id)
    {
        $resource = $request->attributes->get('resource');
        $resource['model']::findOrFail($id)->delete();
        return redirect()->route('magicrud.index', ['resource' => $resource['URI']]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        $resource = $request->attributes->get('resource');
        if (isset($resource['validator'])) {
            $request->validate($resource['validator']);
        }
        $entity = $resource['model']::create($request->all());
        $this->doUpdate($entity, $resource, $request);
        return redirect()->route('magicrud.index', ['resource' => $resource['URI']]);
    }

    protected function doUpdate($entity, $resource, $request)
    {
        foreach ($resource['form'] as $field) {
            if (!empty($field['multiple'])) {
                $entity->{$field['id']}()->sync($request->get($field['id']));
            }
            if ($field['type'] === 'file') {
                $file = $request->file($field['id']);
                $actualName = $file->getClientOriginalName();
                $path = 'images/' . $this->getYearMonth();
                if (Storage::disk('public')->putFileAs($path, $file, $actualName)) {
                    $entity->{$field['id']} = $path . '/' . $actualName;
                }
            }
        }
        $entity->save();
        event(new MagiCrudUpdateEntity($entity, $request));
    }

    /**
     * return a string of year/date.
     *
     * @return String
     */
    private function getYearMonth()
    {
        $date = Carbon::now();
        return $date->year . '/' . $date->month;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param  $id
     * @return Response
     */
    public function update(Request $request, $resource, $id)
    {
        $resource = $request->attributes->get('resource');
        if (isset($resource['validator'])) {
            $request->validate($resource['validator']);
        }
        $entity = $resource['model']::findOrFail($id);
        $entity->update($request->all());
        $this->doUpdate($entity, $resource, $request);
        return redirect()->route('magicrud.index', ['resource' => $resource['URI']]);
    }
}
