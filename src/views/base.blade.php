<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no"/>
    <title>@yield('title')</title>
    <meta name="description" content="SoundOfNews admin"/>
    <link rel="shortcut icon" href="favicon.ico">
    <link rel="icon" href="favicon.ico" type="image/x-icon">
    <link href="/vendor/magicrud/css/select2.min.css" rel="stylesheet" type="text/css"/>
    <link href="/vendor/magicrud/css/style.css" rel="stylesheet" type="text/css">
    <link href="/vendor/magicrud/css/open-editor.css" rel="stylesheet" type="text/css">
    <link href="/vendor/magicrud/css/dropify.min.css" rel="stylesheet" type="text/css">
    <link href="/vendor/magicrud/css/gallery-selector.css" rel="stylesheet" type="text/css">
    <link href='https://cdn.jsdelivr.net/npm/froala-editor@3.0.6/css/froala_editor.pkgd.min.css' rel='stylesheet' type='text/css' />


</head>
<body>
<div class="hk-wrapper hk-vertical-nav">
    <nav class="navbar navbar-expand-xl navbar-dark fixed-top hk-navbar">
        <a id="navbar_toggle_btn" class="navbar-toggle-btn nav-link-hover" href="javascript:void(0);"><span
                class="feather-icon"><i data-feather="menu"></i></span></a>
        <ul class="navbar-nav hk-navbar-content">
            <li class="nav-item dropdown dropdown-authentication">
                <a class="nav-link dropdown-toggle no-caret" href="#" role="button" data-toggle="dropdown"
                   aria-haspopup="true" aria-expanded="false">
                    <div class="media">
                        <div class="media-body">
                            <span>{{ Auth::user()->name }}<i class="zmdi zmdi-chevron-down"></i></span>
                        </div>
                    </div>
                </a>
                <div class="dropdown-menu dropdown-menu-right" data-dropdown-in="flipInX" data-dropdown-out="flipOutX">
                    <a class="dropdown-item" href="{{ route('logout') }}"
                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                        <i class="dropdown-icon zmdi zmdi-power"></i><span>Log out</span>
                    </a>
                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                        @csrf
                    </form>
                </div>
            </li>
        </ul>
    </nav>
    <!-- /Top Navbar -->

    <!-- Vertical Nav -->
    <nav class="hk-nav hk-nav-light">
        <a href="javascript:void(0);" id="hk_nav_close" class="hk-nav-close"><span class="feather-icon"><i
                    data-feather="x"></i></span></a>
        <div class="nicescroll-bar">
            <div class="navbar-nav-wrap" style="overflow-x: hidden">
                <div class="nav-header">
                    <span>Administration</span>
                    <span>Ad</span>
                </div>
                <ul class="navbar-nav flex-column">
                    @foreach($adminMenu as $id => $menuItem)
                        <li class="nav-item">
                            <a class="nav-link" href="javascript:void(0);" data-toggle="collapse"
                               data-target="#nav_drop_{{$id}}" aria-expanded="false">
                                <span class="feather-icon"><i data-feather="{{$menuItem['icon']}}"></i></span>
                                <span class="nav-link-text">{{$menuItem['label']}}</span>
                            </a>
                            <ul id="nav_drop_{{$id}}" class="nav flex-column collapse-level-1 collapse" style="">
                                <li class="nav-item">
                                    <ul class="nav flex-column">
                                        <li class="nav-item">
                                            <a class="nav-link"
                                               href="{{ route('magicrud.index', ['resource'=>$id]) }}">Liste</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link"
                                               href="{{ route('magicrud.create', ['resource'=>$id]) }}">Ajouter</a>
                                        </li>
                                    </ul>
                                </li>
                            </ul>
                        </li>
                    @endforeach
                </ul>
            </div>
        </div>
    </nav>
    <div id="hk_nav_backdrop" class="hk-nav-backdrop"></div>
    <!-- /Vertical Nav -->

    <!-- Setting Panel -->
    <!-- /Setting Panel -->

    <!-- Main Content -->
    <div class="hk-pg-wrapper">
        <!-- Breadcrumb -->
        <nav class="hk-breadcrumb" aria-label="breadcrumb">
            <ol class="breadcrumb breadcrumb-light bg-transparent">
                @hasSection('prevLink')
                    <li class="breadcrumb-item">@yield('prevLink')</li>
                    <li class="breadcrumb-item active" aria-current="page">@yield('breadcrumb')</li>
                @else
                    <li class="breadcrumb-item active" aria-current="page">@yield('breadcrumb')</li>
                @endif
            </ol>
        </nav>
        <!-- /Breadcrumb -->

        <!-- Container -->
        <div class="container">
            <!-- Title -->
            <div class="hk-pg-header">
                <h4 class="hk-pg-title">@yield('title')</h4>
            </div>
            <!-- /Title -->

            <!-- Row -->
            <div class="row">
                <div class="col-xl-12">
                    <section class="hk-sec-wrapper">
                        <div class="row">
                            @yield('body')
                        </div>
                    </section>

                </div>
            </div>
            <!-- /Row -->

        </div>
        <!-- /Container -->

        <!-- Footer -->
        <div class="hk-footer-wrap container">
            <footer class="footer">
                <div class="row">
                    <div class="col-md-6 col-sm-12">
                        <p>Sound of News</p>
                    </div>
                </div>
            </footer>
        </div>
        <!-- /Footer -->

    </div>
    <!-- /Main Content -->

</div>
<!-- HK Wrapper -->

<!-- jQuery -->
<script src="/vendor/magicrud/js/gallery-selector.js"></script>
<script src="/vendor/magicrud/js/jquery.min.js"></script>
<script src="/vendor/magicrud/js/popper.min.js"></script>
<script src="/vendor/magicrud/js/bootstrap.min.js"></script>
<script src="/vendor/magicrud/js/feather.min.js"></script>
<script src="/vendor/magicrud/js/dropdown-bootstrap-extended.js"></script>
<script src="/vendor/magicrud/js/select2.full.min.js"></script>
<script src="/vendor/magicrud/js/init.js"></script>
<script src="/vendor/magicrud/js/open-editor.js"></script>
<script type='text/javascript' src='https://cdn.jsdelivr.net/npm/froala-editor@3.0.6/js/froala_editor.pkgd.min.js'></script>
@stack("scripts")
</body>
</html>
