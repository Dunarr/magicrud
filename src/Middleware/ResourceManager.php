<?php

namespace Dunarr\MagiCrud\Middleware;

use Closure;

class ResourceManager
{
    /**
     * List of authorized IPs
     *
     * @var array $authorizedIPs
     */
    protected $authorizedIPs = ['127.0.0.1'];
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $resourceName = $request->route()->parameter('resource');
        $resources = config('magicrud.resources');
        $resource = $resources[$resourceName]??false;
        if($resource){
            $resource['URI'] = $resourceName;
            $request->attributes->add(compact("resource"));
            return $next($request);
        }
        return abort('404');
    }
}
