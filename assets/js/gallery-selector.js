class GallerySelector {
  constructor(el) {
    this.popup = document.createElement("div");
    this.popup.classList.add("image-picker-popup");
    this.select = el;
    this.pics = {};
    this.button = document.createElement("div");
    this.button.classList.add("image-picker-button");
    this.button.classList.add("form-control");
    this.button.addEventListener("click", this.displayPopup());

    const options = el.options;
    const inner = document.createElement("div");
    inner.classList.add("inner");
    for (let i = 0; i < options.length; i++) {
      const option = options[i];
      this.pics[option.value] = option.innerText;
      const picture = document.createElement("div");
      picture.classList.add("image-picker-button");
      picture.classList.add("form-control");
      picture.style.backgroundImage = `url('/storage/${option.innerText}')`;
      picture.addEventListener("click", this.changeValue(option.value, option.innerText));
      inner.append(picture)
      if(option.selected){
        this.button.style.backgroundImage = `url('/storage/${option.innerText}')`;
      }
    }
    this.popup.append(inner);
    el.style.display = "none";

    el.parentNode.insertBefore(this.button, el.nextSibling);
    this.body = document.querySelector("body")

  }

  displayPopup() {
    return () => this.body.append(this.popup)
  }

  hidePopup() {
    this.popup.remove()
  }

  changeValue(i, url) {
    return () => {
      this.hidePopup();
      this.button.style.backgroundImage = `url('/storage/${url}')`;
      this.select.value = i
    }
  }
}

function initGallerySelector(selector) {
  const result = [];
  const els = document.querySelectorAll(selector);
  for (let i = 0; i < els.length; i++) {
    result.push(new GallerySelector(els[i]))
  }
  return result
}

initGallerySelector(".gallery-selector");
