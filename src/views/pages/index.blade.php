@extends("magicrud::base")
@section('title', $label)
@section('body')
    <div class="col-sm">
        <div class="table-wrap">
            <div class="table-responsive">
                <table class="table table-lg mb-0">
                    <thead>
                    <tr>
                        <th>{{ $label }}</th>
                        <th>Actions</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($entities as $entity)
                        <tr>
                            <td>{{ $entity }}</td>
                            <td>
{{--                                <a href="{{ route('magicrud.show', ['id'=>$entity->id,'resource'=>$uri]) }}" class="mr-25" data-toggle="tooltip" data-original-title="Voir"> <i class="icon-eye txt-primary"></i> </a>--}}
                                <a href="{{ route('magicrud.edit', ['id'=>$entity->id,'resource'=>$uri]) }}" class="mr-25" data-toggle="tooltip" data-original-title="Modifier"> <i class="icon-pencil txt-primary"></i> </a>
                                {!! Form::open(['method'=>'DELETE', 'route'=>['magicrud.destroy',$uri, $entity->id], 'class'=>'inline-form']) !!}
                                <button  type="submit" data-toggle="tooltip" data-original-title="Supprimer" class="invisible-button"> <i class="icon-trash txt-danger"></i> </button>
                                {!! Form::close() !!}
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
        {{ $entities->links() }}
    </div>
@endsection
@section('breadcrumb', \Illuminate\Support\Pluralizer::plural($label))
