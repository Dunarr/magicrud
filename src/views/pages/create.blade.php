@extends("magicrud::base")
@section('title', 'new '.$label)
@section('body')
    {!! Form::open(['route' => ['magicrud.store', $uri], 'class'=>'col-md-12', 'files' => $formOptions['files']]) !!}
    @foreach($form as $field)
        @component('magicrud::input/'.$field['type'], $field)@endcomponent
    @endforeach
    {!! Form::submit('save', ['class' => 'btn btn-primary']) !!}
    {!! Form::close() !!}
@endsection
@section('breadcrumb', 'Nouveau')
@section('prevLink')
    <a href="{{ route('magicrud.index', ['resource'=>$uri])}}">{{ \Illuminate\Support\Pluralizer::plural($label) }}</a>
@endsection
@push('scripts')
    <script src="/vendor/magicrud/js/dropify.min.js"></script>
    <script src="/vendor/magicrud/js/editor.js"></script>
@endpush
