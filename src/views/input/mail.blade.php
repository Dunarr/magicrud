<div class="form-group">
    <label for="{{ $id }}">{{ $label }}</label>
    {!! Form::email($id,null, ['class' => "form-control square-input mt-15",'id' => $id]) !!}
    @error($id)
    <div class="text-danger">{{$message}}</div>
    @enderror
</div>
