<?php namespace Dunarr\MagiCrud\Composers;

class MagiMenuComposer
{
    public function compose($view)
    {
        //Add your variables
        $view->with('adminMenu',
                config('magicrud.resources')
            );
    }
}
