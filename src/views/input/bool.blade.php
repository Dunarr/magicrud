<div class="custom-control custom-checkbox">
    {!! Form::checkbox($id,'1', null, ['id' => $id, 'class' => 'custom-control-input']) !!}
    <label class="custom-control-label" for="{{ $id }}">{{ $label }}</label>
    @error($id)
    <div class="text-danger">{{$message}}</div>
    @enderror
</div>
