<?php

use Dunarr\MagiCrud\Controllers\CRUDController;

Route::name('magicrud.')->prefix('admin')->middleware(['ressource_manager', 'web', 'auth'])->group(function(){
    Route::resource('/{resource}/', CRUDController::class)->parameters(['' => 'id']);;
});
