<div class="form-group">
    <label for="{{ $id }}">{{ $label }}</label>
    <div class="row">
        <div class="col-sm">
            {!! Form::file($id, ['class' => "dropify",'id' => $id]) !!}
        </div>
    </div>
    @error($id)
    <div class="text-danger">{{$message}}</div>
    @enderror
</div>
