<?php

namespace Dunarr\MagiCrud\Events;

use Illuminate\Http\Request;
use Illuminate\Queue\SerializesModels;

class MagiCrudDisplayIndex
{
    use SerializesModels;

    public $request;
    public $resource;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(Request $request, Array $resource)
    {
        $this->request = $request;
        $this->resource = $resource;
    }
}
